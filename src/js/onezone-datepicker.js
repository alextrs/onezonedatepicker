angular.module('onezone-datepicker', ['ionic', 'onezone-datepicker.templates', 'onezone-datepicker.service'])
    .directive('onezoneDatepicker', ['$ionicGesture', 'onezoneDatepickerService', function ($ionicGesture, onezoneDatepickerService) {
        'use strict';

        function drawDatepicker(scope) {
            var selectedDates = {}, lastDate, now, parameters = {};

            /* SET SELECTED DATE */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.date) && angular.isDate(scope.datepickerObject.date)) {
                selectedDates[onezoneDatepickerService.getDateHash(scope.datepickerObject.date)] = angular.copy(scope.datepickerObject.date);
            } else if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.date) && angular.isNumber(scope.datepickerObject.date)) {
                selectedDates[onezoneDatepickerService.getDateHash(scope.datepickerObject.date)] = new Date(scope.datepickerObject.date);
            } else if (angular.isDefined(scope.datepickerObject) && angular.isArray(scope.datepickerObject.date)) {
                selectedDates = onezoneDatepickerService.dateArrToMap(scope.datepickerObject.date, selectedDates);
            } else {
                now = new Date();
                selectedDates[onezoneDatepickerService.getDateHash(now)] = now;
            }

            lastDate = onezoneDatepickerService.getLastDate(selectedDates);
            scope.selectedDates = selectedDates;
            scope.currentMonth = angular.copy(lastDate);

            parameters = onezoneDatepickerService.getParameters(scope);

            /* CREATE MONTH CALENDAR */
            scope.createDatepicker = function (date) {
                var createMonthParam = {
                    date: date,
                    mondayFirst: parameters.mondayFirst,
                    disablePastDays: parameters.disablePastDays,
                    displayFrom: parameters.displayFrom,
                    displayTo: parameters.displayTo,
                    disableWeekend: parameters.disableWeekend,
                    disableDates: parameters.disableDates,
                    disableDaysOfWeek: parameters.disableDaysOfWeek,
                    highlights: parameters.highlights
                };

                return onezoneDatepickerService.createMonth(createMonthParam);
            };

            scope.month = scope.createDatepicker(lastDate);
            scope.yearSlides = onezoneDatepickerService.getYears(parameters.startYear, parameters.endYear);
            scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
            return parameters;
        }

        function showHideDatepicker(scope, value) {
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.showDatepicker)) {
                scope.datepickerObject.showDatepicker = value;
            } else {
                scope.datepicker.showDatepicker = value;
            }
        }

        function setDate(scope, parameters) {
            var selectedDatesArr = onezoneDatepickerService.dateMapToArr(scope.selectedDates), assignDate;

            if (parameters.multiSelect) {
                assignDate = selectedDatesArr;
            } else {
                assignDate = selectedDatesArr[0];
            }

            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.date)) {
                scope.datepickerObject.date = assignDate;
            }

            if (!parameters.calendarMode) {
                showHideDatepicker(scope, false);
            }

            if (angular.isDefined(parameters.callback)) {
                parameters.callback(assignDate);
            }
        }

        var link = function (scope, element, attrs) {
            var parameters = {};

            scope.datepicker = {
                showDatepicker: false,
                showLoader: false,
                showMonthModal: false,
                showYearModal: false,
                showTodayButton: false,
                calendarMode: false,
                hideCancelButton: false,
                hideSetButton: false
            };

            parameters = drawDatepicker(scope);
            scope.datepicker.showDatepicker = parameters.showDatepicker || parameters.calendarMode;
            scope.datepicker.calendarMode = parameters.calendarMode;
            scope.datepicker.hideCancelButton = parameters.hideCancelButton;
            scope.datepicker.hideSetButton = parameters.hideSetButton;
            scope.datepicker.showTodayButton = onezoneDatepickerService.showTodayButton(parameters);

            /* VERIFY THAT PROVIDED DATE IS SELECTED */
            scope.isDateSelected = function (date) {
                return scope.selectedDates[onezoneDatepickerService.getDateHash(date)] ? true : false;
            };

            /* SELECT DATE METHOD */
            scope.selectDate = function (date, isDisabled) {
                var dateHash;
                if (!isDisabled) {
                    dateHash = onezoneDatepickerService.getDateHash(date);
                    if (!scope.selectedDates[dateHash]) {
                        if (!parameters.multiSelect && Object.keys(scope.selectedDates).length > 0) {
                            onezoneDatepickerService.resetDateMap(scope.selectedDates);
                        }
                        scope.selectedDates[dateHash] = date;
                    } else if (parameters.multiSelect) {
                        delete scope.selectedDates[dateHash];
                    }
                    scope.month = scope.createDatepicker(date);
                    scope.currentMonth = angular.copy(date);
                    scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());

                    if (parameters.calendarMode || parameters.hideSetButton) {
                        setDate(scope, parameters);
                    }
                }
            };

            /* SELECT TODAY METHOD */
            scope.selectToday = function () {
                scope.selectDate(new Date());
            };

            /* SELECT MONTH METHOD */
            scope.selectMonth = function (monthIndex) {
                scope.currentMonth = new Date(scope.currentMonth.getFullYear(), monthIndex, 1);
                scope.month = scope.createDatepicker(scope.currentMonth);
                scope.closeModals();
            };

            /* SELECT YEAR METHOD */
            scope.selectYear = function (year) {
                scope.currentMonth = new Date(year, scope.currentMonth.getMonth(), 1);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.closeYearModal();
            };

            /* NEXT MONTH METHOD */
            scope.nextMonth = function () {
                scope.currentMonth = new Date(scope.currentMonth.getFullYear(), scope.currentMonth.getMonth() + 1, 1);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.month = scope.createDatepicker(scope.currentMonth);
            };

            /* PREVIOUS MOTH METHOD */
            scope.previousMonth = function () {
                scope.currentMonth = onezoneDatepickerService.getPreviousMonth(scope.currentMonth);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.month = scope.createDatepicker(scope.currentMonth);
            };

            scope.swipeLeft = function () {
                if (!parameters.disableSwipe) {
                    scope.nextMonth();
                }
            };

            scope.swipeRight = function () {
                if (!parameters.disableSwipe) {
                    scope.previousMonth();
                }
            };

            /* CLOSE MODAL */
            scope.closeModals = function () {
                scope.datepicker.showMonthModal = false;
                scope.datepicker.showYearModal = false;
            };

            /* OPEN SELECT MONTH MODAL */
            scope.openMonthModal = function () {
                scope.datepicker.showMonthModal = true;
            };

            /* OPEN SELECT YEAR MODAL */
            scope.openYearModal = function () {
                scope.datepicker.showMonthModal = false;
                scope.datepicker.showYearModal = true;
            };

            /* CLOSE SELECT MONTH MODAL */
            scope.closeYearModal = function () {
                scope.datepicker.showYearModal = false;
                scope.datepicker.showMonthModal = true;
            };

            scope.showDatepicker = function () {
                if (!scope.datepicker.showDatepicker) {
                    showHideDatepicker(scope, true);
                }
            };

            scope.hideDatepicker = function () {
                showHideDatepicker(scope, false);
            };

            scope.setDate = function () {
                setDate(scope, parameters);
            };

            scope.$watch('datepickerObject.date', function (date) {
                var lastDate;
                if (angular.isNumber(date)) {
                    date = new Date(date);
                }

                onezoneDatepickerService.resetDateMap(scope.selectedDates);
                if (angular.isDate(date)) { 
                    date = [date]; 
                }  
                if (angular.isArray(date)) {
                    scope.selectedDates = onezoneDatepickerService.dateArrToMap(date, scope.selectedDates);
                    lastDate = onezoneDatepickerService.getLastDate(scope.selectedDates);
                    scope.month = scope.createDatepicker(lastDate);
                    scope.currentMonth = angular.copy(lastDate);
                    scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                }
            });

            scope.$watch('datepickerObject.showDatepicker', function (value) {
                scope.datepicker.showDatepicker = value;
            });
            
            scope.$watchCollection('datepickerObject.highlights', function () { 
                drawDatepicker(scope); 
            });

            scope.$watchCollection('datepickerObject.disableDates', function () { 
                drawDatepicker(scope); 
            });

            element.on("click", function ($event) {
                var target = $event.target;
                if (angular.isDefined(target) && angular.element(target).hasClass("show-onezone-datepicker")) {
                    scope.$apply(function () {
                        drawDatepicker(scope);
                        scope.showDatepicker();
                    });
                }
            });
        };

        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            link: link,
            scope: {
                datepickerObject: '=datepickerObject'
            },
            //templateUrl: 'lib/onezone-datepicker/src/templates/onezone-datepicker.html'
            templateUrl: 'onezone-datepicker.html'
        };
    }]);